Summary:        System for manage the build process in an operating system
Name:           cmake
Version:        2.8.12.2
Release:        1
License:        BSD
Group:          Development Tools
URL:            http://www.cmake.org/
Source0:        http://www.cmake.org/files/v2.8/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmpdir}/%{name}-%{version}-%{release}-root-%(id -u -n)

%description
Open-source system that manages the build process in an operating system
and in a compiler-independent manner.

%prep
%setup -q

%build
%configure \
	--prefix=/usr \
	--datadir=share/%{name}-2.8 \
	--docdir=share/doc/%{name}-2.8 \
	--mandir=share/man
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/usr/bin/ctest
/usr/bin/cmake
/usr/bin/cpack
/usr/share/%{name}-2.8/*
/usr/share/doc/%{name}-2.8/*
/usr/share/man/*
/usr/share/aclocal/cmake.m4

%changelog

